package fr.ecomaison.radioactivite.domain.features

import fr.ecomaison.radioactivite.domain.model.RadioActivityCriticityLevel.*
import fr.ecomaison.radioactivite.domain.model.RadioactivityMesure
import fr.ecomaison.radioactivite.domain.model.bqperm3

class ComputeCriticityLevel {

    operator fun invoke(mesure: RadioactivityMesure) = when {
        mesure > "150.00".bqperm3() -> CRITICAL
        mesure > "100.00".bqperm3() -> HIGH
        mesure > "50.00".bqperm3() -> MEDIUM
        else -> LOW
    }

}