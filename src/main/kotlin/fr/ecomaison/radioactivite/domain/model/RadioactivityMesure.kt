package fr.ecomaison.radioactivite.domain.model

import java.math.BigDecimal
import java.math.RoundingMode

data class RadioactivityMesure(var value: BigDecimal, val unit: RadioactivityUnit = RadioactivityUnit.BQ_PER_M3) {

    init{
        if (value < BigDecimal.ZERO) {
            throw RadioactivityMesureBadValueException(value)
        }
        this.value = value.setScale(4, RoundingMode.HALF_DOWN)
    }
    operator fun compareTo(radioactivityMesure: RadioactivityMesure): Int {
        return value.compareTo(radioactivityMesure.value)
    }

}
    fun String.bqperm3(): RadioactivityMesure {
        return RadioactivityMesure(toBigDecimal())
    }
