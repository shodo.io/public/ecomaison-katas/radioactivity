package fr.ecomaison.radioactivite.domain.model

enum class RadioactivityUnit {
    BQ_PER_M3
}
