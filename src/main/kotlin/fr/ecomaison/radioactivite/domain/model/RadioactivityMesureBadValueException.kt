package fr.ecomaison.radioactivite.domain.model

import java.math.BigDecimal

class RadioactivityMesureBadValueException(val value: BigDecimal) :
    RuntimeException("$value is a valid radioactivity mesure")
