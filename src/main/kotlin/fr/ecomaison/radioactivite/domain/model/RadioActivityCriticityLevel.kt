package fr.ecomaison.radioactivite.domain.model

enum class RadioActivityCriticityLevel {
    LOW, MEDIUM, HIGH, CRITICAL
}
