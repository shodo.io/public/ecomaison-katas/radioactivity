package fr.ecomaison.radioactivite.domain.features

import fr.ecomaison.radioactivite.domain.model.RadioActivityCriticityLevel
import fr.ecomaison.radioactivite.domain.model.RadioActivityCriticityLevel.*
import fr.ecomaison.radioactivite.domain.model.RadioactivityMesure
import fr.ecomaison.radioactivite.domain.model.bqperm3
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class RadioactiviteTest {

    private val computeCriticityLevel = ComputeCriticityLevel()

    @Test
    fun `should return low criticity level when mesure is bewteen 0 and 50 bq per m3`() {
        criticityLevelOf("0.00".bqperm3()).shouldBe(LOW)
        criticityLevelOf("50.00".bqperm3()).shouldBe(LOW)
    }

    @Test
    fun `should return medium criticity level when mesure is between 50 bq per m3 and 100 bq per m3`() {
        criticityLevelOf("50.01".bqperm3()).shouldBe(MEDIUM)
        criticityLevelOf("100.00".bqperm3()).shouldBe(MEDIUM)
    }

    @Test
    fun `should return high criticity level when mesure is between 100 bq per m3 and 150 bq per m3`() {
        criticityLevelOf("100.01".bqperm3()).shouldBe(HIGH)
        criticityLevelOf("150.00".bqperm3()).shouldBe(HIGH)
    }

    @Test
    fun `should return critical level when mesure is higher than 150 bq per m3 `() {
        criticityLevelOf("150.01".bqperm3()).shouldBe(CRITICAL)
    }

    private fun criticityLevelOf(mesure: RadioactivityMesure) = computeCriticityLevel(mesure)

}
private fun RadioActivityCriticityLevel.shouldBe(level: RadioActivityCriticityLevel) = Assertions.assertThat(this).isEqualTo(level)