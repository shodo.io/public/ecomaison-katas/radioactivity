package fr.ecomaison.radioactivite.domain.model

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class RadioactivityMesureTest{

    @Test
    fun negative_value_should_not_be_accepted() {
        Assertions.assertThatCode { RadioactivityMesure("-10.00".toBigDecimal()) }.isInstanceOf(RadioactivityMesureBadValueException::class.java)
    }
}