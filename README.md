# Problem Description
![](Carte.png)

Nous travaillons pour le gouvernement français et une directive européenne 
nous impose de montrer l'état de la radioactivité sur le territoire français par département.

Pour cela nous comptons associer une couleur à un niveau de criticité selon les règles suivantes :

* JAUNE : 0 <= mesure <= 50
* MARRON : 51 <= mesure <= 100
* ORANGE : 101 <= mesure <= 150
* ROUGE : 151 <= mesure

Le but de ce kata est de créer une fonction qui applique ces différentes règles à une mesure.
